<?php
/**
 * Template Name: Staff Template
 * Description: Custom page template.
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>
	<div class="staff row" role="main">
		<img class="line" src="<?php bloginfo('template_directory'); ?>/img/images/line.png" alt="" >
		<div class="small-12 medium-8 large-8 columns">
			<?php
				if(have_posts()) {
					while(have_posts()) {
						the_post();
						echo '<h2 class="page-title">Our '.get_the_title().'</h2>';
						the_content();
						// comments_template( '', true );
					}
				}
			?>
			<?php if (have_posts()) : while (have_posts()) : the_post();
				the_content();
			endwhile; endif; ?>

			<?php 
				$post_type = 'staff';
				$post_args = array(
					'post_type' => $post_type,
					'posts_per_page' => -1,
					'orderby' => 'date',
					'order' => 'DESC'
				);

				$posts = new WP_Query($post_args);
				if($posts->have_posts()){
					while($posts->have_posts()){
						$posts->the_post();

						$staff_desc = wpautop( get_post_meta( get_the_ID(), '_cwmb_staff_bio', true ) );
						$img_src= get_post_meta( get_the_ID(), '_cwmb_staff_image', true );
						$staff_title= get_post_meta( get_the_ID(), '_cwmb_staff_title', true );

						echo '<div class="row staff_content"><div class="small-12 medium-3 large-3 columns" >';
							echo'<img class="staff_pic" src="'.$img_src.'">';
						echo '</div>';
						echo '<div class="small-12 medium-9 large-9 columns" >';
							echo '<h3>';
								the_title();
							echo '</h3>';
							echo '<h5>'.$staff_title.'</h5>';
							echo $staff_desc;
						echo '</div></div><hr>';
						
						// the_post_thumbnail();
						// the_content();


						// echo '<pre>';
						// print_r(get_post_meta($post->ID));
						// echo '</pre>';

					}
				} else {
					echo '<p>No '.$post_type.' yet. Check back soon</p>';
				}

				wp_reset_query();
			?>
		</div>

		<?php get_sidebar(); ?>

	</div>

<?php get_footer(); ?>