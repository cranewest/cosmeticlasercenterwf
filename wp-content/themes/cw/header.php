<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
?><!DOCTYPE html>
<!--[if lt IE 7]> <html style="margin-top: 0!important;" class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>    <html style="margin-top: 0!important;" class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>    <html style="margin-top: 0!important;" class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html style="margin-top: 0!important;" class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">

	<title><?php wp_title( '|', true, 'right' ); ?></title>

	<?php
		$favicon = cw_options_get_option( 'cwo_favicon' );
		if(!empty($favicon)) { echo '<link rel="shortcut icon" type="image/png" href="'.$favicon.'"/>'; }
	?>

	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="author" href="<?php echo get_template_directory_uri(); ?>/humans.txt">
	<link rel="dns-prefetch" href="//ajax.googleapis.com">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

	<!-- WP_HEAD() -->
	<?php wp_head(); ?>

	<!--[if lt IE 9]>
		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/ie.css">
	<![endif]-->
</head>

<body <?php body_class(); ?>>
<div class="banner_bg">
	<header role="banner">
		<div class="row ">
			<div class="small-12 medium-6 large-6 columns">
				<?php $logo_url = cw_options_get_option( 'cwo_logo' ); ?>
				<h1 class="logo">
					<a href="/" title="<?php bloginfo( 'name' ); ?>">
						<?php if(!empty($logo_url)) { ?>
							<img src="<?php echo $logo_url; ?>" alt="<?php bloginfo( 'name' ); ?>" />
						<?php } else {
							bloginfo( 'name' );
						} ?>
					</a>
				</h1>
			</div>

			<div class="small-12 medium-6 large-6  columns">
				<span class="slogan pull-right">Texoma's Most Experienced</span>

				<div class="row con_info">
					<div class="small-7 medium-8 large-9 columns">
						<span class="grey_txt f_right">FREE CONSULTATION</span><br>
						<span class="phone_txt f_right"><h5><?php echo '<a href="tel:'.preg_replace("/[^0-9]/","",cw_options_get_option( 'cwo_phone' )).'">'.cw_options_get_option( 'cwo_phone' ).'</a>';?></h5></span>
					</div>

					<div class="small-5 medium-4 large-3 columns">
						<span class="grey_txt f_right">FOLLOW US</span><br>
						<a target="_blank" class="f_right" href="https://www.facebook.com/cosmeticlasercenter" title="Like us on Facebook"><img class="social_icon"  src="<?php bloginfo('template_directory'); ?>/img/images/fb-icon.png" alt="like us on facebook"></a>
						<a target="_blank" class="f_right" href="https://www.twitter.com/LaserCenterWF" title="Follow us on Twitter"><img class="social_icon"  src="<?php bloginfo('template_directory'); ?>/img/images/twitter-icon.png" alt="follow us on twitter"></a>
						<a target="_blank" class="f_right" href="http://www.youtube.com/user/LaserCenterWF" title="Follow us on YouTube"><img  class="social_icon" src="<?php bloginfo('template_directory'); ?>/img/images/youtube-video.png" alt="follow us on YouTube"></a>
					</div>
					<img class="line_social" src="<?php bloginfo('template_directory'); ?>/img/images/line.png" alt="" >

				</div>

			</div>

			
		</div>	

		<div class="nav-container contain-to-grid">
			<nav class="top-bar" data-topbar role="navigation">
				<ul class="title-area">
					<li class="name"></li>
					<li class="toggle-topbar menu-icon"><a href="#">Menu</a></li>
				</ul>


				<section class="top-bar-section">
					<?php 
						wp_nav_menu(
							array(
								'theme_location' => 'primary',
								'container' => '',
								'menu_class' => 'menu',
								'depth' => 2,
								'fallback_cb' => 'wp_page_menu',
								'walker' => new Foundation_Walker_Nav_Menu()
							)
						);
					?>

				</section>
			</nav>
		</div>
	</header>
</div>

	<?php //get_template_part('content', 'slides'); ?>
