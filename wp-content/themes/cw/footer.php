<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
?>
<div class="footer_area">
	<footer role="contentinfo" class="row">
		<div class="small-12 medium-4 large-4 columns">
			<p >
				&copy; <?php echo date('Y'); ?> <?php bloginfo( 'name' ); ?> <br>
				<a href="/privacy-policy">Privacy Policy</a><br>

			</p>
		</div>
		<div class="small-12 medium-4 large-4 columns">
			<!-- <a class="cw" href="http://crane-west.com/"><img src="<?php echo get_template_directory_uri(); ?>/img/cw-light.png" alt="Site Powered by Crane | West" /></a> -->
			<p class="bbb center">
				<a title="Click for the Business Review of The Cosmetic Laser Center, a Skin Care in Wichita Falls TX" href="http://www.bbb.org/wichita-falls/business-reviews/skin-care/the-cosmetic-laser-center-in-wichita-falls-tx-80409#sealclick"><img alt="Click for the BBB Business Review of this Skin Care in Wichita Falls TX" style="border: 0; float: none;" src="http://seal-wichitafalls.bbb.org/seals/black-seal-120-61-thecosmeticlasercenter-80409.png"></a>
			</p>
		</div>
		<div class="small-12 medium-4 large-4 columns f_right">
			<!-- <a class="cw" href="http://crane-west.com/"><img src="<?php echo get_template_directory_uri(); ?>/img/cw-light.png" alt="Site Powered by Crane | West" /></a> -->
			<p class="address">
				<?$patsys_title = str_replace("\'", "'",cw_options_get_option( 'cwo_title' ));?>
				<strong><?php echo $patsys_title;?><br></strong>
				<?php echo cw_options_get_option( 'cwo_address1' );?><br>
				<?php echo cw_options_get_option( 'cwo_city' );?>,
				<?php echo cw_options_get_option( 'cwo_state' );?>&nbsp;
				<?php echo cw_options_get_option( 'cwo_zip' );?><br>
				<?php echo '<a href="tel:'.preg_replace("/[^0-9]/","",cw_options_get_option( 'cwo_phone' )).'">'.cw_options_get_option( 'cwo_phone' ).'</a>';?>
				<!-- <a href="tel:19408726284"><strong>1(940)872-6284</strong></a> -->
				<p>
					Monday thru Friday<br>
					8:00am - 5:00pm<br>
				</p>
			</p>
		</div>
	</footer>
</div>

	<!-- WP_FOOTER() -->
	<?php wp_footer(); ?>
	<?php
		// must activate CW Options Page plugin for the line below to work
		$ga_code = cw_options_get_option('cwo_ga'); if( !empty($ga_code) ) {
	?>
	<script type="text/javascript">
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', '<?php echo $ga_code; ?>', 'auto');
		ga('send', 'pageview');
	</script>
	<?php } ?>
</body>
</html>
