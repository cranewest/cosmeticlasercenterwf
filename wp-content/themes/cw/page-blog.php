<?php
/* Template Name: Blog Page*/
?>
<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>

	<div class="main row" role="main">
		<img class="line" src="<?php bloginfo('template_directory'); ?>/img/images/line.png" alt="" >
		<div class="small-12 medium-8 large-8 columns">
			<?php
				if(have_posts()) {
					while(have_posts()) {
						the_post();
						echo '<h2 class="page-title">Latest '.get_the_title().'s</h2>';
						the_content();
						// comments_template( '', true );
					}
				}
			?>
 
			<?php 	
				//adjusting the query
				$args = array(
					// 'category_name' => 'news',
					'post_type' => 'post',
					'posts_per_page' => -1,
					// 'orderby' => 'menu_order'
				);

				// The Query
				$latest_post = new WP_Query( $args );
				// The Loop
				if ( $latest_post->have_posts() ) 
				{
					while ( $latest_post->have_posts() ) 
					{	
						$latest_post->the_post();
						
						// echo'<div class = "">';
						// echo'<div class = "small-12 medium-12 large-12 columns post_content"><br>';
							$n_link = get_post_meta($post->ID, '_cwmb_link', true);
							if($n_link)
							{
								?><a target="_blank" href="<? echo $n_link; ?>"><strong><h3 class="each_post_title"><? echo get_the_title(); ?></h3></strong></a><br><?
							}
							else{
								?><a href="<? the_permalink(); ?>"><strong><h3 class="each_post_title"><? echo get_the_title(); ?></h3></strong></a><br><?
							}
							
			
							echo'<div class=""><span class="blog_thumb">';
								the_post_thumbnail();
								echo '</span>';
								the_excerpt();
								// the_content();
							echo'</div>';

							if($n_link)
							{
								?><a class="f_right" target="_blank" href="<?php echo $n_link; ?>">Read Article»</a><?
							}
							else{
								?><a class="f_right" href="<?php the_permalink(); ?>">Read Article»</a><br><?
							}
						// echo'</div></div>';
							echo '<hr class="blog_hr"><div class="row date_area"><div class="small-12 medium-7 large-7 columns">';
							    $social_sharing_toolkit = new MR_Social_Sharing_Toolkit();
							    echo $social_sharing_toolkit->create_bookmarks();
							echo'</div>';
							echo '<div class="small-12 medium-5 large-5 columns date">';
								echo '<div class="f_right">';
							    echo the_time('F j, Y - g:i a');
							    echo '</div>';
							echo'</div></div>';

							echo '<hr>';
						
					}
				} 
				else 
				{
					// no posts do nothing
				}

				/* Restore original Post Data */
				wp_reset_postdata();
			?>

		</div>

		<?php get_sidebar(); ?>
	</div>

<?php get_footer(); ?>