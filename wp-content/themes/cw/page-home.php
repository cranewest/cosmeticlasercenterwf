<?php
/* Template Name: Home Page*/
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>

	<div class="home_page_title_bg">
		<div class="row slide_area">
			<img class="line" src="<?php bloginfo('template_directory'); ?>/img/images/line.png" alt="" >
			<div class="small-12 medium-8 large-8 columns slide_content">
				<?php get_template_part('content', 'slides'); ?>
				<img class="slide_shadow" src="<?php bloginfo('template_directory'); ?>/img/images/slider-shadow.png" alt="" >

				<h3 class="clc_blog">CLC Blog</h3>

				<div class="small-12 medium-6 large-6 columns blog_home_content">
					<?php 	
						//adjusting the query
						$args = array(
							// 'category_name' => 'news',
							'post_type' => 'post',
							'posts_per_page' => 1,
							// 'orderby' => 'menu_order'
						);

						// The Query
						$latest_post = new WP_Query( $args );
						// The Loop
						if ( $latest_post->have_posts() ) 
						{
							while ( $latest_post->have_posts() ) 
							{	
								$latest_post->the_post();
								
								// echo'<div class = "">';
								// echo'<div class = "small-12 medium-12 large-12 columns post_content"><br>';
									$n_link = get_post_meta($post->ID, '_cwmb_link', true);
									if($n_link)
									{
										?><a target="_blank" href="<? echo $n_link; ?>"><strong><h4 class="each_post_title"><? echo get_the_title(); ?></h4></strong></a><br><?
									}
									else{
										?><a href="<? the_permalink(); ?>"><strong><h4 class="each_post_title"><? echo get_the_title(); ?></h4></strong></a><br><?
									}
									
					
									echo'<div class=""><span class="blog_thumb_home">';
										the_post_thumbnail();
										echo '</span><br>';
										the_excerpt();
										// the_content();
									echo'</div>';

									if($n_link)
									{
										?><a class="f_right" target="_blank" href="<?php echo $n_link; ?>">Read Article»</a><?
									}
									else{
										?><a class="f_right" href="<?php the_permalink(); ?>">Read more</a><br><?
									}
								
								
							}
						} 
						else 
						{
							// no posts do nothing
						}

						/* Restore original Post Data */
						wp_reset_postdata();
					?>
				</div>

				<div class="small-12 medium-6 large-6 columns blog_home_content">
					<h6><strong>MORE ARTICLES</strong></h6>
					<?php 	
						//adjusting the query
						$args = array(
							// 'category_name' => 'news',
							'post_type' => 'post',
							'posts_per_page' => -6,
							// 'orderby' => 'menu_order'
						);

						// The Query
						$latest_post = new WP_Query( $args );
						// The Loop
						if ( $latest_post->have_posts() ) 
						{
							$count=0;
							while ( $latest_post->have_posts() ) 
							{	
								$latest_post->the_post();
								
								// echo'<div class = "">';
								// echo'<div class = "small-12 medium-12 large-12 columns post_content"><br>';
								if($count>0){
									$n_link = get_post_meta($post->ID, '_cwmb_link', true);
									echo '<div class="home_article">';
									if($n_link)
									{
										?><a target="_blank" href="<? echo $n_link; ?>"><strong><h6 class="each_post_title"><? echo get_the_title(); ?></h6></strong></a><br><?
									}
									else{
										?><a href="<? the_permalink(); ?>"><strong><h6 class="each_post_title"><? echo get_the_title(); ?></h6></strong></a><br><?
									}
									echo '</div>';
								}
								$count++;
								
								
							}
						} 
						else 
						{
							// no posts do nothing
						}

						/* Restore original Post Data */
						wp_reset_postdata();
					?>
					<br><a href="/blog/" class="button article_button">Read All</a>
				</div>

			</div>
			<div class="small-12 medium-4 large-4 columns slide_home_content">


				<?php 	
					//adjusting the query
					$taxonomies = get_object_taxonomies( (object) array( 'post_type' => 'promos') );

					$args = array(
						'post_type' => 'promos',
						'posts_per_page' => -1,
						'order' => DESC,
						'tax_query' => array(
							array(
								'taxonomy' => $taxonomies[0],
								'field' => 'slug',
								'terms' => 'call-for-appointment'
							)
						)
						
					);

					// The Query
					$latest_post = new WP_Query( $args );
					// The Loop
					if ( $latest_post->have_posts() ) 
					{
						$count=0;
						while ( $latest_post->have_posts() ) 
						{	
							$latest_post->the_post();
							$link = get_post_meta($post->ID, '_cwmb_promo_link', true);
							$img_src = get_post_meta($post->ID, '_cwmb_promo_image', true);
					
							$loc = get_post_meta( $post->ID, '_cwmb_location', true);

							if ($count==0){
								if ($link){
									echo'<a target="_blank" href="'.$link.'">'; 
									?>
										<img class="" src="<? echo $img_src; ?>"> 
										<img class="promo_shadow" src="<?php bloginfo('template_directory'); ?>/img/images/home_sidebar_promo_shadow.png" alt="" >

									<?
									echo'</a>';

								} 
								else{
									?>
										<img class="" src="<? echo $img_src; ?>"> 
										<img class="promo_shadow" src="<?php bloginfo('template_directory'); ?>/img/images/home_sidebar_promo_shadow.png" alt="" >
									<?
								}
								$count++;
							 }
							
						}
					} 
					else 
					{
						// no posts do nothing
					}

					/* Restore original Post Data */
					wp_reset_postdata();
				?>

				
				<?php 	
					//adjusting the query
					$taxonomies = get_object_taxonomies( (object) array( 'post_type' => 'promos') );

					$args = array(
						'post_type' => 'promos',
						'posts_per_page' => -1,
						'orderby' => 'menu_order',
						'tax_query' => array(
							array(
								'taxonomy' => $taxonomies[0],
								'field' => 'slug',
								'terms' => 'sidebar'
							)
						)
						
					);

					// The Query
					$latest_post = new WP_Query( $args );
					// The Loop
					if ( $latest_post->have_posts() ) 
					{
						$count=0;
						while ( $latest_post->have_posts() ) 
						{	
							$latest_post->the_post();
							$link = get_post_meta($post->ID, '_cwmb_promo_link', true);
							$img_src = get_post_meta($post->ID, '_cwmb_promo_image', true);
					
							$loc = get_post_meta( $post->ID, '_cwmb_location', true);

							if ($count<=2){
								if ($link){
									echo'<a target="_blank" href="'.$link.'">'; 
									?><img class="promo" src="<? echo $img_src; ?>"> <?
									echo'</a>';

								} 
								else{
									?><img class="promo" src="<? echo $img_src; ?>"> <?
								}
								$count++;
							}
							
						}
					} 
					else 
					{
						// no posts do nothing
					}

					/* Restore original Post Data */
					wp_reset_postdata();
				?>
				<div class="signup">
					Sign up for <span class="so"> Special Offers </span>
					<?php echo do_shortcode('[gravityform id="3" name="Offer Signup " title="false" description="false"]'); ?>
				</div>
		
			</div>
		</div>

	</div>

	<div class="main row" role="main">
		

		<?php //get_sidebar(); ?>
	</div>

<?php get_footer(); ?>