<?php
/* Template Name: Products Skin Care Page*/
?>
<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>

<div role="main" class="staff-cats row">
	<img class="line" src="<?php bloginfo('template_directory'); ?>/img/images/line.png" alt="" >
		<div class="small-12 medium-8 large-8 columns">

			<h2 class="page-title">Products
				<ul class="procedure-list">
					<li><a href="/products/" title="All Products" >All</a></li>
					<li><a href="/product-skin-care/" class="current" title="Skin Care">Skin Care</a></li>
					<li><a href="/product-makeup"  title=" Makeup"> Makeup</a></li>
					<li><a href="/product-wellness"   title="Wellness">Wellness</a></li>
				</ul>
			</h2>
			
			<!-- get categories -->
			<?php
				// $category_id='face';
				// $category_link = get_category_link( $category_id );
				// print_r( $category_link);

				$post_type = 'products';
				$taxonomies = get_object_taxonomies( (object) array( 'post_type' => $post_type) );

				// get category terms
				$terms = get_terms( $taxonomy );
				?>
					<!-- display the terms -->
					<h3 class="section-header">Skin Care</h3><hr>
					<div>
					<!-- get the posts -->

					<?php

						$args = array(
							'post_type' => $post_type,
							'posts_per_page' => -1,
							'orderby' => 'title',
							'order' => 'ASC',
							'tax_query' => array(
								array(
									'taxonomy' => $taxonomies[0],
									'field' => 'slug',
									'terms' => 'skin-care'
								)
							)
						);
						$posts = new WP_Query( $args );
						if( $posts->have_posts() ) {
							while( $posts->have_posts() ) {
								$posts->the_post(); ?>
								<div class="listing large-12 columns">
									<div class="inner">

										<!-- check for image -->
										<?php if( has_post_thumbnail() ) { ?><figure class="listing-image"><?php the_post_thumbnail() ?></figure><?php }?>
										<div class="listing-info<?php if( has_post_thumbnail() ) { ?> has-image<?php }?>">
											<!-- all of the info, if it exists -->
											<!-- <a href="<?php the_permalink()?>"><h4 class="procedures_title"><?php the_title() ?></h4></a> -->
											<h4 class="procedures_title"><?php the_title() ?></h4>
											<?
												$products_desc = wpautop( get_post_meta( get_the_ID(), '_cwmb_products_desc', true ) );
												$products_title= get_post_meta( get_the_ID(), '_cwmb_products_title', true );

												echo '<h6 class="procedures_sub_title"><i><strong>'.$products_title.'</strong></i></h6>';
												echo $products_desc;

											?>
										</div><hr>
									</div>
								</div>
							<?php } // end while have_posts
						} // end if have_posts ?>
					</div>

			<? wp_reset_query(); ?>
		</div>

		<?php get_sidebar(); ?>

	</div>

<?php get_footer(); ?>