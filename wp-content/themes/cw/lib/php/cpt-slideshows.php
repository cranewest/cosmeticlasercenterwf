<?php
/**
 * Slideshows Custom Post Type
 *
 * @since CW 1.0
 */
function cw_cpp_slideshows_init() {
	$field_args = array(
		'labels' => array(
			'name' => __( 'Slideshows' ),
			'singular_name' => __( 'Slideshows' ),
			'add_new' => __( 'Add New Slideshow' ),
			'add_new_item' => __( 'Add New Slideshow' ),
			'edit_item' => __( 'Edit Slideshow' ),
			'new_item' => __( 'Add New Slideshow' ),
			'view_item' => __( 'View Slideshow' ),
			'search_items' => __( 'Search Slideshows' ),
			'not_found' => __( 'No slideshows found' ),
			'not_found_in_trash' => __( 'No slideshows found in trash' )
		),
		'public' => true,
		'show_ui' => true,
		'capability_type' => 'post',
		'hierarchical' => true,
		'rewrite' => true,
		'menu_position' => 20,
		'supports' => array('title'),
        'menu_icon' => 'dashicons-images-alt'
	);
	register_post_type('slideshows',$field_args);
}
add_action( 'init', 'cw_cpp_slideshows_init' );



// /**
//  * Add Categories to Custom Post Type
//  *
//  * This is just an example of adding categories to a custom post type.
//  * To see in action just uncomment and it will add categories to slideshows.
//  *
//  * @since CW 1.0
//  */
// // function cw_cpp_slideshows_categories() {
// // 	$field_args = array(
// // 		'labels' => array(
// // 			'name'              => _x( 'Categories', 'taxonomy general name' ),
// // 			'singular_name'     => _x( 'Category', 'taxonomy singular name' ),
// // 			'search_items'      => __( 'Search Categories' ),
// // 			'all_items'         => __( 'All Categories' ),
// // 			'parent_item'       => __( 'Parent Category' ),
// // 			'parent_item_colon' => __( 'Parent Category:' ),
// // 			'edit_item'         => __( 'Edit Category' ),
// // 			'update_item'       => __( 'Update Category' ),
// // 			'add_new_item'      => __( 'Add New Category' ),
// // 			'new_item_name'     => __( 'New Category' ),
// // 			'menu_name'         => __( 'Categories' ),
// // 		),
// // 		'hierarchical' => true
// // 	);
// // 	register_taxonomy( 'slideshows_categories', 'slideshows', $field_args );
// // }
// // add_action( 'init', 'cw_cpp_slideshows_categories', 0 );