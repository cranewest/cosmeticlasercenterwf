<?php
/**
 * Procedures Custom Post Type
 *
 * @since CW 1.0
 */
function cw_cpp_procedures_init() {
	$field_args = array(
		'labels' => array(
			'name' => __( 'Procedures' ),
			'singular_name' => __( 'Procedures' ),
			'add_new' => __( 'Add New Listing' ),
			'add_new_item' => __( 'Add New Listing' ),
			'edit_item' => __( 'Edit Listing' ),
			'new_item' => __( 'Add New Listing' ),
			'view_item' => __( 'View Listing' ),
			'search_items' => __( 'Search Procedures' ),
			'not_found' => __( 'No listings found' ),
			'not_found_in_trash' => __( 'No listings found in trash' )
		),
		'public' => true,
		'show_ui' => true,
		'capability_type' => 'post',
		'hierarchical' => true,
		'rewrite' => true,
		'menu_position' => 20,
		'supports' => array('title'),
        'menu_icon' => 'dashicons-admin-users'
	);
	register_post_type('procedures',$field_args);
}
add_action( 'init', 'cw_cpp_procedures_init' );



/**
 * Tweaks to Featured Image on Staff
 *
 * Moves Featured Image field from sidebar to main on staff and renames it.
 *
 * @since CW 1.0
 */
// function cw_cpp_staff_move_image_box() {
// 	remove_meta_box( 'postimagediv', 'staff', 'side' );
// 	add_meta_box('postimagediv', __('Listing Image'), 'post_thumbnail_meta_box', 'staff', 'normal', 'high');
// }
// add_action('do_meta_boxes', 'cw_cpp_staff_move_image_box');



/**
 * Add Categories to Custom Post Type
 *
 * This is just an example of adding categories to a custom post type.
 * To see in action just uncomment and it will add categories to staff.
 *
 * @since CW 1.0
 */
function cw_cpp_procedures_categories() {
	$field_args = array(
		'labels' => array(
			'name'              => _x( 'Categories', 'taxonomy general name' ),
			'singular_name'     => _x( 'Category', 'taxonomy singular name' ),
			'search_items'      => __( 'Search Categories' ),
			'all_items'         => __( 'All Categories' ),
			'parent_item'       => __( 'Parent Category' ),
			'parent_item_colon' => __( 'Parent Category:' ),
			'edit_item'         => __( 'Edit Category' ),
			'update_item'       => __( 'Update Category' ),
			'add_new_item'      => __( 'Add New Category' ),
			'new_item_name'     => __( 'New Category' ),
			'menu_name'         => __( 'Categories' ),
		),
		'hierarchical' => true
	);
	register_taxonomy( 'procedures_categories', 'procedures', $field_args );
}
add_action( 'init', 'cw_cpp_procedures_categories', 0 );