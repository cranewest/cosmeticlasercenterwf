<?php
/* Template Name: Contact Page*/
?>
<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>

	<div class="main row" role="main">
		<img class="line" src="<?php bloginfo('template_directory'); ?>/img/images/line.png" alt="" >
		<div class="small-12 medium-8 large-8 columns">
			<?php
				if(have_posts()) {
					while(have_posts()) {
						the_post();
						echo '<h2 class="page-title">'.get_the_title().'</h2>';
						the_content();
						// comments_template( '', true );
					}
				}
			?>
 
			<div class="google-maps">
				<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d828.0238674578703!2d-98.511685!3d33.887195!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x865320440b3bd711%3A0x6a65cde954082567!2s2611+Harrison+St%2C+Wichita+Falls%2C+TX+76308!5e0!3m2!1sen!2sus!4v1434486591569" width="600" height="450" frameborder="0" style="border:0"></iframe>
			</div>
			<?php echo do_shortcode('[gravityform id="1" name="Contact" title="false" description="false"]'); ?>

		</div>

		<?php get_sidebar(); ?>
	</div>

<?php get_footer(); ?>