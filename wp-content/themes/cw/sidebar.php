<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
?>
	<aside class="widget-area small-12 medium-4 large-4 columns" role="complementary">
		<?php //dynamic_sidebar( 'sidebar-1' ); ?>

		<?php 	
			//adjusting the query
			$taxonomies = get_object_taxonomies( (object) array( 'post_type' => 'promos') );

			$args = array(
				'post_type' => 'promos',
				'posts_per_page' => -1,
				'order' => DESC,
				'tax_query' => array(
					array(
						'taxonomy' => $taxonomies[0],
						'field' => 'slug',
						'terms' => 'call-for-appointment'
					)
				)
				
			);

			// The Query
			$latest_post = new WP_Query( $args );
			// The Loop
			if ( $latest_post->have_posts() ) 
			{
				$count=0;
				while ( $latest_post->have_posts() ) 
				{	
					$latest_post->the_post();
					$link = get_post_meta($post->ID, '_cwmb_promo_link', true);
					$img_src = get_post_meta($post->ID, '_cwmb_promo_image', true);
			
					$loc = get_post_meta( $post->ID, '_cwmb_location', true);

					if ($count==0){
						if ($link){
							echo'<a target="_blank" href="'.$link.'">';
							$rand_num = mt_rand(1, 3);
							if ($rand_num > 1)
							{
								?>
									<img class="promo_lady" src="<?php bloginfo('template_directory'); ?>/img/images/lady-0<? echo $rand_num;?>.png" alt="" >
									<img class="" src="<? echo $img_src; ?>"> 
									<img class="promo_shadow" src="<?php bloginfo('template_directory'); ?>/img/images/home_sidebar_promo_shadow.png" alt="" >
								<?
							}
							else{
								?>
									<img class="promo_lady1" src="<?php bloginfo('template_directory'); ?>/img/images/lady-0<? echo $rand_num;?>.png" alt="" >
									<img class="" src="<? echo $img_src; ?>"> 
									<img class="promo_shadow" src="<?php bloginfo('template_directory'); ?>/img/images/home_sidebar_promo_shadow.png" alt="" >
								<?
							}
							echo'</a>';

						} 
						else{
							?>
								<img class="" src="<? echo $img_src; ?>"> 
								<img class="promo_shadow" src="<?php bloginfo('template_directory'); ?>/img/images/home_sidebar_promo_shadow.png" alt="" >
							<?
						}
						$count++;
					}
					
				}
			} 
			else 
			{
				// no posts do nothing
			}

			/* Restore original Post Data */
			wp_reset_postdata();
		?>

		<?php 	
			//adjusting the query
			$taxonomies = get_object_taxonomies( (object) array( 'post_type' => 'promos') );

			$args = array(
				'post_type' => 'promos',
				'posts_per_page' => -1,
				'orderby' => 'menu_order',
				'tax_query' => array(
					array(
						'taxonomy' => $taxonomies[0],
						'field' => 'slug',
						'terms' => 'sidebar'
					)
				)
				
			);

			// The Query
			$latest_post = new WP_Query( $args );
			// The Loop
			if ( $latest_post->have_posts() ) 
			{
				$count=0;
				while ( $latest_post->have_posts() ) 
				{	
					$latest_post->the_post();
					$link = get_post_meta($post->ID, '_cwmb_promo_link', true);
					$img_src = get_post_meta($post->ID, '_cwmb_promo_image', true);
			
					$loc = get_post_meta( $post->ID, '_cwmb_location', true);

					if ($count<=1){
						if ($link){
							echo'<a target="_blank" href="'.$link.'">'; 
							?><img class="promo" src="<? echo $img_src; ?>"> <?
							echo'</a>';

						} 
						else{
							?><img class="promo" src="<? echo $img_src; ?>"> <?
						}
						$count++;
					}
					
				}
			} 
			else 
			{
				// no posts do nothing
			}

			/* Restore original Post Data */
			wp_reset_postdata();
		?>
		<div class="signup">
			Sign up for <span class="so"> Special Offers </span>
			<?php echo do_shortcode('[gravityform id="3" name="Offer Signup " title="false" description="false"]'); ?>
		</div>

	</aside>


