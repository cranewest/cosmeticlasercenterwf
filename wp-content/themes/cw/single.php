<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>

	<div class="main row" role="main">
		<img class="line" src="<?php bloginfo('template_directory'); ?>/img/images/line.png" alt="" >
		<div class="small-12 medium-8 large-8 columns">
			<?php while ( have_posts() ) : the_post(); ?>
				<h2 class="page-title_page">Latest Blogs</h2>

				<?php 
					echo '<h2 class="post-title">'.get_the_title().'</h2>';
					// get_template_part( 'content', get_post_format() ); 
					the_content();
					echo do_shortcode('[fbcomments]');
				?>

				<?php //comments_template( '', true ); ?>

			<?php endwhile; // end of the loop. ?>
		</div>

		<?php get_sidebar(); ?>
	</div>

<?php get_footer(); ?>